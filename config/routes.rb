Soapclient::Application.routes.draw do
  mount RailsAdmin::Engine => '/admin', :as => 'rails_admin'
  devise_for :users
  root 'home#index'

  resources :countries, only: :index
  resources :visited_countries, except: :show
end
