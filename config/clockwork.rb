require 'rubygems'
require './config/boot'
require './config/environment'
require 'clockwork'
include Clockwork

module Clockwork
  configure do |config|
    config[:sleep_timeout] = 5
    config[:max_threads] = 15
    config[:thread] = true
  end

  handler do |job|
    puts "Running #{job}"
  end

  # handler receives the time when job is prepared to run in the 2nd argument
  # handler do |job, time|
  #   puts "Running #{job}, at #{time}"
  # end


  every(1.day, 'load:country') { system 'rake load:country' }

end