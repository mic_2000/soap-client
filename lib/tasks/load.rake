
namespace :load do

  desc "Loading information about the countries and currencies"
  task :country => :environment do

    puts 'Start loading...'

    client = Savon.client(wsdl: 'http://country-soap-srv.herokuapp.com/countries/wsdl')
    response = call_and_fail_gracefully(client, :soap_country)

    if response.success?
      country_with_money = eval(response.body[:soap_country_response][:value])
      if country_with_money
        country_with_money.each do |country|
          Country.where(name: country["country_name"], code: country["country_code"]).first_or_create!({
            name:          country["country_name"],
            code:          country["country_code"],
            money_name:    country["money_name"],
            money_code:    country["money_code"],
            money_symbol:  country["money_symbol"]
          })
        end
      end
    end

    puts 'Completed'

  end


  def call_and_fail_gracefully(client, *args, &block)
    client.call(*args, &block)
  rescue Savon::SOAPFault => e
    puts e.message
  end

end