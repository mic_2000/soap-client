class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.string :code, limit: 5
      t.string :money_name
      t.string :money_code, limit: 5
      t.string :money_symbol, limit: 5

      t.timestamps
    end
    add_index :countries, [:name, :money_name], :unique => true
  end
end
