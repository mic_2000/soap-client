class CreateVisitedCountries < ActiveRecord::Migration
  def change
    create_table :visited_countries do |t|
      t.integer :user_id
      t.integer :country_id
      t.date :start_date
      t.date :end_date
      t.string :description

      t.timestamps
    end
    add_index :visited_countries, :user_id
  end
end
