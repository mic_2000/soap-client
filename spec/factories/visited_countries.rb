# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :visited_country do
    association :user, factory: :user
    association :country, factory: :country
    sequence(:start_date) { |n| Date.today + n.day }
    sequence(:end_date) { |n| Date.today + n.day}
    description "MyString"
  end
end
