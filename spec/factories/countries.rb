# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country do
    sequence(:name) { |n| "Country name #{n}" }
    code "AF"
    sequence(:money_name) { |n| "Money name #{n}" }
    sequence(:money_code) { |n| "#{n}" }
    sequence(:money_symbol) { |n| "#{n}" }
  end
end
