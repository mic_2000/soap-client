require 'spec_helper'

describe Country do

  before(:each) do
    @country1 = create(:country, money_name: 'Euro', money_code: 'Eu')
    @country2 = create(:country, money_name: 'Dollars', money_code: 'US')
    @country3 = create(:country, money_name: 'Euro', money_code: 'Eu')
    @country4 = create(:country, money_name: 'Dollars', money_code: 'US')
  end

  it "should distinct data by field and order by field" do
    field = :money_name
    countries = Country.distinct(field)
    countries.map(&field).should == ['Dollars', 'Euro']
  end

  it "should show non visited coutries and non collect money" do
    user = create(:user)
    visited_country1 = create(:visited_country, country_id: @country1.id, user_id: user.id,
                              start_date: Date.today - 5.day, end_date: Date.today - 5.day)
    visited_country2 = create(:visited_country, country_id: @country2.id, user_id: user.id,
                              start_date: Date.today + 5.day, end_date: Date.today + 5.day)
    countries = Country.non_visited_countries(user.id)
    countries.should == [@country2, @country4]
  end
end
