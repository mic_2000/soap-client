require 'spec_helper'

describe Ability do
  before(:each) do
    @user = create(:user)
    @admin = create(:admin)
    @visited_country1 = create(:visited_country, user_id: @user.id)
    @visited_country2 = create(:visited_country)
  end

  it "user should manage own visited coutries" do
    Ability.new(@user).should be_able_to(:manage, @visited_country1)
  end

  it "user should not manage other visited coutries" do
    Ability.new(@user).should_not be_able_to(:manage, @visited_country2)
  end

  it "admin should access to rails_admin" do
    Ability.new(@admin).should_not be_able_to(:manage, @visited_country2)
  end
end