require 'spec_helper'

describe VisitedCountry do
  it { should belong_to(:country) }
  it { should belong_to(:user) }
  it { should validate_uniqueness_of(:country_id).scoped_to(:user_id) }

  it "end date must not be less than the start date" do
    @visited_country = create(:visited_country)
    @visited_country.start_date = Date.today
    @visited_country.end_date = Date.today - 1.day
    @visited_country.save
    @visited_country.end_date.should == Date.today
  end

  it "travel date not be the same with other trips" do
    user = create(:user)
    @visited_country1 = create(:visited_country, user_id: user.id,
                               start_date: Date.today - 3.day, end_date: Date.today - 1.days)
    @visited_country2 = create(:visited_country, user_id: user.id)
    @visited_country2.start_date = Date.today - 2.day
    @visited_country2.end_date = Date.today - 1.day
    @visited_country2.save
    @visited_country2.errors.messages.should == {:start_date=>["At this time you ride into #{@visited_country1.country.name}"]}
  end
end
