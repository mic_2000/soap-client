require 'spec_helper'

describe User do
  it { should have_many(:visited_countries) }

  it "should return name user from email" do
    user = create(:user, email: 'user@domen.com')
    user.name.should == 'User'
  end
end
