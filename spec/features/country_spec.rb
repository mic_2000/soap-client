require 'spec_helper'

describe Country do
  context "set filter" do
    before(:each) do
      @countries1 = create_list(:country, 10, money_name: 'Euro', money_code: 'EU')
      @australia = create(:country, name: 'Australia', money_name: 'Euro', money_code: 'Eu')
      @countries2 = create_list(:country, 5, money_name: 'Dollars', money_code: 'US')
    end

    it "should show country with currencies Dollars", js: true do
      visit countries_path
      page.select 'Dollars', :from => 'money_name'
      sleep(3)
      page.all("tr.data-row").count.should == 5
    end

    it "should show country with currencies Euro and name Australia", js: true do
      visit countries_path
      page.select 'Australia', :from => 'name'
      page.select 'Euro', :from => 'money_name'
      sleep(3)
      page.all("tr.data-row").count.should == 1
    end
  end
end