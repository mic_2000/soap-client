require 'spec_helper'

describe VisitedCountry do
  context "set filter" do
    before(:each) do
      @user = create(:user)
      sign_in @user
      @countries1 = create_list(:country, 10, money_name: 'Euro', money_code: 'EU')
      @countries2 = create_list(:country, 5, money_name: 'Dollars', money_code: 'US')
    end

    it "should show country with currencies Dollars", js: true do
      visit visited_countries_path
      page.select 'Dollars', :from => 'money_name'
      sleep(3)
      page.all("tr.data-row").count.should == 5
    end
  end

  context "make travel" do
    before(:each) do
      @user = create(:user)
      sign_in @user
      @countries1 = create_list(:country, 10, money_name: 'Euro', money_code: 'EU')
      @australia = create(:country, name: 'Australia', money_name: 'Euro', money_code: 'Eu')
      @countries2 = create_list(:country, 5, money_name: 'Dollars', money_code: 'US')
    end

    it "should add planned travell to Australia with currencies Dollars", js: true do
      visit visited_countries_path
      click_link('Australia')
      fill_in('Description', :with => 'cool travel')
      fill_in('Start date', :with => Date.today + 1.day)
      fill_in('End date', :with => Date.today + 2.day)
      click_button('Save')
      sleep(3)
      within(".all-countries") do
        page.has_content?('Australia').should be_true
      end
      within(".visited") do
        page.has_content?('Australia').should be_true
      end
    end

    it "should add travelled to Australia with collect currencies Dollars", js: true do
      visit visited_countries_path
      click_link('Australia')
      fill_in('Description', :with => 'cool travel')
      fill_in('Start date', :with => Date.today - 2.day)
      fill_in('End date', :with => Date.today - 1.day)
      click_button('Save')
      sleep(3)
      within(".all-countries") do
        page.has_content?('Australia').should_not be_true
      end
      within(".visited") do
        page.has_content?('Australia').should be_true
        page.has_css?('tr.data-visited.success').should be_true
      end
    end
  end

end