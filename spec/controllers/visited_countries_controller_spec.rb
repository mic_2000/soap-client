require 'spec_helper'

describe VisitedCountriesController do
  describe "GET index" do
    context "for guest user" do
      it "assigns all countries with user_id in params" do
        user1 = create(:user)
        user2 = create(:user)
        countries_user1 = create_list(:visited_country, 5, user_id: user1.id)
        countries_user2 = create_list(:visited_country, 5, user_id: user2.id)
        get :index, {user_id: user1.id}
        assigns(:visited_countries).should eq(countries_user1)
      end
    end

    context "for authorized user" do
      before(:each) do
        @user = create(:user)
        sign_in @user
        @countries_user1 = create_list(:visited_country, 5, user_id: @user.id)
        @countries = create_list(:country, 5)
        @visited_country1 = create(:visited_country, user_id: @user.id,
                                   start_date: Date.today - 3.day, end_date: Date.today - 1.days)
      end

      it "assigns all visited_countries as @visited_countries" do
        get :index, {}
        assigns(:visited_countries).should eq([@visited_country1] + @countries_user1)
      end

      it "assigns all non visited countries as @visited_countries" do
        get :index, {}
        country_planned = @countries_user1.map {|cu| cu.country}
        assigns(:non_visited_countries).should eq(country_planned + @countries)
      end

      it "assigns all user countries (visited and planned)" do
        get :index, {}
        country_planned = @countries_user1.map {|cu| cu.country}
        assigns(:all_user_countries).should eq(country_planned.map(&:id) + [@visited_country1.country_id])
      end
    end
  end

  context "for authorized user" do
    before(:each) do
      @user = create(:user)
      sign_in @user
    end

    describe "GET new" do
      it "assigns a new visited_country as @visited_country" do
        get :new, format: :js
        assigns(:visited_country).should be_a_new(VisitedCountry)
      end
    end

    describe "GET edit" do
      it "assigns the requested visited_country as @visited_country" do
        visited_country = create(:visited_country, user_id: @user.id)
        get :edit, {:id => visited_country.to_param, format: :js}
        assigns(:visited_country).should eq(visited_country)
      end
    end

    describe "POST create" do
      before(:each) do
        @countries = create_list(:country, 5)
      end
      let(:valid_attributes) { { country_id: @countries.first.id, start_date: Date.today,
                                 end_date: Date.today + 1.day, description: 'description' } }
      describe "with valid params" do
        it "creates a new VisitedCountry" do
          expect {
            post :create, {:visited_country => valid_attributes, format: :js}
          }.to change(VisitedCountry, :count).by(1)
        end

        it "assigns a newly created visited_country as @visited_country" do
          post :create, {:visited_country => valid_attributes, format: :js}
          assigns(:visited_country).should be_a(VisitedCountry)
          assigns(:visited_country).should be_persisted
        end

        it "redirects to the created visited_country" do
          post :create, {:visited_country => valid_attributes, format: :js}
          response.should render_template("index")
        end
      end

      describe "with invalid params" do
        it "assigns a newly created but unsaved visited_country as @visited_country" do
          VisitedCountry.any_instance.stub(:save).and_return(false)
          post :create, {:visited_country => valid_attributes, format: :js}
          assigns(:visited_country).should be_a_new(VisitedCountry)
        end

        it "re-renders the 'new' template" do
          VisitedCountry.any_instance.stub(:save).and_return(false)
          post :create, {:visited_country => valid_attributes, format: :js}
          response.should render_template("new")
        end
      end
    end

    describe "PUT update" do
      before(:each) do
        @visited_country = create(:visited_country, user_id: @user.id)
      end
      let(:valid_attributes) { { start_date: Date.today,
                                 end_date: Date.today + 1.day, description: 'description1' } }

      describe "with valid params" do
        it "updates the requested visited_country" do
          VisitedCountry.any_instance.should_receive(:update).with({ "description" => "params" })
          put :update, {:id => @visited_country.to_param, :visited_country => { "description" => "params" }, format: :js}
        end

        it "assigns the requested visited_country as @visited_country" do
          put :update, {:id => @visited_country.to_param, :visited_country => valid_attributes, format: :js}
          assigns(:visited_country).should eq(@visited_country)
        end

        it "redirects to the visited_country" do
          put :update, {:id => @visited_country.to_param, :visited_country => valid_attributes, format: :js}
          response.should render_template("index")
        end
      end

      describe "with invalid params" do
        it "assigns the visited_country as @visited_country" do
          VisitedCountry.any_instance.stub(:save).and_return(false)
          put :update, {:id => @visited_country.to_param, :visited_country => valid_attributes, format: :js}
          assigns(:visited_country).should eq(@visited_country)
        end

        it "re-renders the 'edit' template" do
          VisitedCountry.any_instance.stub(:save).and_return(false)
          put :update, {:id => @visited_country.to_param, :visited_country => valid_attributes, format: :js}
          response.should render_template("new")
        end
      end
    end

    describe "DELETE destroy" do
      it "destroys the requested visited_country" do
        visited_country = create(:visited_country, user_id: @user.id)
        expect {
          delete :destroy, {:id => visited_country.to_param}
        }.to change(VisitedCountry, :count).by(-1)
      end

      it "redirects to the visited_countries list" do
        visited_country = create(:visited_country, user_id: @user.id)
        delete :destroy, {:id => visited_country.to_param}
        response.should redirect_to(visited_countries_url)
      end
    end
  end
end
