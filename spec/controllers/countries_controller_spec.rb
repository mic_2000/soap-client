require 'spec_helper'

describe CountriesController do

  describe "GET index" do
    context "for guest user" do
      it "assigns all countries as @countries" do
        countries = create_list(:country, 5)
        get :index, {}
        assigns(:countries).should eq(countries)
      end
    end

    context "for authorized user" do
      before(:each) do
        @user = create(:user)
        sign_in @user
        @country1 = create(:country, name: 'Country 1', money_name: 'Euro', money_code: 'Eu')
        @country2 = create(:country, name: 'Country 2', money_name: 'Dollars', money_code: 'US')
        @country3 = create(:country, name: 'Country 3', money_name: 'Euro', money_code: 'Eu')
        @country4 = create(:country, name: 'Country 4', money_name: 'Dinars', money_code: 'DZD')
        @country5 = create(:country, name: 'Country 5', money_name: 'Dollars', money_code: 'US')
        @country6 = create(:country, name: 'Country 6', money_name: 'Dinars', money_code: 'DZD')
        @visited_country1 = create(:visited_country, user_id: @user.id, country_id: @country1.id,
                                   start_date: Date.today - 3.day, end_date: Date.today - 1.days)
        @visited_country2 = create(:visited_country, user_id: @user.id, country_id: @country2.id,
                                   start_date: Date.today + 3.day, end_date: Date.today + 3.days)
      end

      it "assigns non visited countries as @countries" do
        get :index, {}
        assigns(:countries).should eq([@country2, @country4, @country5, @country6])
      end

      it "assigns filter by money_name for non visited countries as @countries" do
        get :index, {money_name: 'Dinars'}
        assigns(:countries).should eq([@country4, @country6])
      end

      it "assigns filter by country_name and money_name for non visited countries as @countries" do
        get :index, {name: @country4.name, money_name: 'Dinars'}
        assigns(:countries).should eq([@country4])
      end
    end
  end

end
