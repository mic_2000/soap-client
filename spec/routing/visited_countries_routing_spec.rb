require "spec_helper"

describe VisitedCountriesController do
  describe "routing" do

    it "routes to #index" do
      get("/visited_countries").should route_to("visited_countries#index")
    end

    it "routes to #new" do
      get("/visited_countries/new").should route_to("visited_countries#new")
    end

    it "routes to #edit" do
      get("/visited_countries/1/edit").should route_to("visited_countries#edit", :id => "1")
    end

    it "routes to #create" do
      post("/visited_countries").should route_to("visited_countries#create")
    end

    it "routes to #update" do
      put("/visited_countries/1").should route_to("visited_countries#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/visited_countries/1").should route_to("visited_countries#destroy", :id => "1")
    end

  end
end
