class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    flash.now[:alert] = exception.message
    redirect_to main_app.root_path
  end

end
