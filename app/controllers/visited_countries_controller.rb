class VisitedCountriesController < ApplicationController
  before_action :set_visited_country, only: [:edit, :update, :destroy]
  authorize_resource except: [:index]

  # GET /visited_countries
  def index
    if params['user_id']
      @visited_countries = (user = User.find params['user_id']) ? user.visited_countries : []
    else
      authenticate_user!
      set_countries
    end
  end

  # GET /visited_countries/new
  def new
    @visited_country = VisitedCountry.new
  end

  # GET /visited_countries/1/edit
  def edit
    render :new
  end

  # POST /visited_countries
  def create
    @visited_country = current_user.visited_countries.new(visited_country_params)
    respond_to do |format|
      if @visited_country.save
        set_countries
        format.html { }
        format.js { render "index" }
      else
        format.html { }
        format.js { render action: "new" }
      end
    end
  end

  # PATCH/PUT /visited_countries/1
  def update
    respond_to do |format|
      if @visited_country.update(visited_country_params)
        set_countries
        format.html { }
        format.js {render "index" }
      else
        format.html { }
        format.js { render action: "new" }
      end
    end
  end

  # DELETE /visited_countries/1
  def destroy
    @visited_country.destroy
    redirect_to visited_countries_url, notice: 'Visited country was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_visited_country
      @visited_country = current_user.visited_countries.find(params[:id])
    end

    def visited_country_params
      params.require(:visited_country).permit(:country_id, :start_date, :end_date, :description)
    end

    def set_countries
      @visited_countries = current_user.visited_countries.order(:end_date)
      @non_visited_countries = Country.non_visited_countries current_user.id
      @all_user_countries = current_user.visited_countries.map(&:country_id) if current_user
    end

end
