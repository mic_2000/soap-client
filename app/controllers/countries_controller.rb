class CountriesController < ApplicationController

  # GET /countries
  def index
    non_visited_countries = current_user.nil? ? Country.all : Country.non_visited_countries(current_user.id)
    @countries = filters(non_visited_countries, params)
    @all_user_countries = current_user.visited_countries.map(&:country_id) if current_user
  end

  private
    def filters countries, params
      fields = [:name, :code, :money_name, :money_code, :money_symbol]
      fields.each do |field|
        countries = countries.where(field => params[field]) unless params[field].blank?
      end
      countries.order(:name)
    end
end
