(function ($) {
  $.fn.aimg = function (options) {

    options = $.extend({}, {
      speed: 150
    }, options);

    return this.each(function () {

      var $el = $(this),
          $img = $('<img src="' + $(this).css('backgroundImage').replace(/(^url)|(["'()])/g, '') + '" style="position: absolute; left: -99999px;">'),
          currFrame = 1,
          slides;

      $img.load(function () {
        slides = $(this).width() / $el.width();
        $(this).remove();
        startAnimation();
      });
      $('body').append($img);

      function startAnimation() {
        return setInterval(function () {

          $el.css('backgroundPosition', '-' + currFrame * $el.width() + 'px 0px');

          if (currFrame == slides) {
            $el.css('backgroundPosition', '0px 0px');
            currFrame = 0;
          }

          currFrame++;
        }, options.speed);
      }
    });
  }
})(jQuery);

$(document).ready(function () {
    $('.spinner').aimg();
});