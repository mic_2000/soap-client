module CountriesHelper
  def filter field
    data = Country.distinct(field).map(&field)
    select_tag(field, options_for_select([""]+data))
  end
end
