class Country < ActiveRecord::Base

  scope :distinct, -> (field) { select(field).distinct.order(field) }
  scope :non_visited_countries, -> (user_id) {
    country = VisitedCountry.where(user_id: user_id).where('end_date < ?', Date.today)
    money_code_ids = country.map {|c| c.country.money_code}
    where(['money_code NOT IN (?)', money_code_ids]) if money_code_ids.any?
  }
end
