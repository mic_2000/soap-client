class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)

    can :access, :rails_admin if user.admin?

    can :read, Country

    can :read, VisitedCountry
    can :create, VisitedCountry, :user_id => user.id
    can :manage, VisitedCountry, :user_id => user.id
  end
end
