class VisitedCountry < ActiveRecord::Base

  belongs_to :country
  belongs_to :user
  validates_uniqueness_of :country_id, :scope => :user_id
  validate :check_visited_date

  def check_visited_date
    vc = VisitedCountry.where(user_id: user_id).where('start_date <= ?', end_date).where('end_date >= ?', start_date)
    vc = vc.where('id <> ?', id) if self.id
    errors.add(:start_date, "At this time you ride into #{vc.first.country.try(:name)}") if vc.first
  end

  def end_date= dt
    self[:end_date] = dt
    self[:end_date] = self[:start_date] if self[:end_date] < self[:start_date]
  end


end
